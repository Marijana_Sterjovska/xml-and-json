<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
<head>
<style type="text/css">
	h1 { text-align: center; margin: 0px auto; display; block; color: #4CAF50;}
	h2 {color: #ff6347; text-decoration: underline;}
	h3 {color: #787878;}
	table {font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; width: 100%;}
	table td, table th {border: 1px solid #ddd; padding: 8px;}
	table tr:nth-child(even){background-color: #f2f2f2; text-align: center;}
	table tr:nth-child(odd){background-color: #ffffff; text-align: center;}
	table tr:hover {background-color: #ddd;}
	table th { padding-top: 12px; padding-bottom: 12px; text-align: center; background-color: #4CAF50; color: white;}
	.left{text-align: left;}
</style>
<title>Education Center</title>
<meta http-equiv="Content-Type"
content="text/html; charset=UTF-8" />
</head>
<body>
<h1><img src="edu_marijana.jpg" width="500" height="310"></img></h1>
<h2>Courses</h2>
<table border="1">
<tr>
<th>Course Id</th>
<th>Title</th>
<th>Duration(hours)</th>
<th>Price(den.)</th>
</tr>
<xsl:for-each select="education_center/course">
<tr>
<td><xsl:value-of select="@id" /></td>
<td class="left"><xsl:value-of select="title" /></td>
<td><xsl:value-of select="duration_hours" /></td>
<td><xsl:value-of select="price_den" /></td>
</tr>
</xsl:for-each>
</table>
<h2>Participants in course: Full Stack with Java </h2>
<h3>Start date: 10.12.2020 </h3>
<table border="1">
<tr>
<th>Participant id</th>
<th>First name</th>
<th>Last name</th>
<th>Email</th>
<th>Phone</th>
<th>City</th>
<th>Country</th>
<th>Role in this course</th>
</tr>
<xsl:for-each select="education_center/course/course_schedule[@start_date='2020-12-10']/participant">
<tr>
<td><xsl:value-of select="@id" /></td>
<td class="left"><xsl:value-of select="first_name" /></td>
<td class="left"><xsl:value-of select="last_name" /></td>
<td class="left"><xsl:value-of select="email" /></td>
<td><xsl:value-of select="phone" /></td>
<td><xsl:value-of select="city" /></td>
<td><xsl:value-of select="country" /></td>
<td><xsl:value-of select="role" /></td>
</tr>
</xsl:for-each>
</table>
<h2>Participants in course: Full Stack with Java </h2>
<h3>Start date: 20.02.2021 </h3>
<table border="1">
<tr>
<th>Participant id</th>
<th>First name</th>
<th>Last name</th>
<th>Email</th>
<th>Phone</th>
<th>City</th>
<th>Country</th>
<th>Role in this course</th>
</tr>
<xsl:for-each select="education_center/course/course_schedule[@start_date='2021-02-20']/participant">
<tr>
<td><xsl:value-of select="@id" /></td>
<td class="left"><xsl:value-of select="first_name" /></td>
<td class="left"><xsl:value-of select="last_name" /></td>
<td class="left"><xsl:value-of select="email" /></td>
<td><xsl:value-of select="phone" /></td>
<td><xsl:value-of select="city" /></td>
<td><xsl:value-of select="country" /></td>
<td><xsl:value-of select="role" /></td>
</tr>
</xsl:for-each>
</table>
<h2>Participants in course: Software Testing </h2>
<h3>Start date: 25.01.2021 </h3>
<table border="1">
<tr>
<th>Participant id</th>
<th>First name</th>
<th>Last name</th>
<th>Email</th>
<th>Phone</th>
<th>City</th>
<th>Country</th>
<th>Role in this course</th>
</tr>
<xsl:for-each select="education_center/course/course_schedule[@start_date='2021-01-25']/participant">
<tr>
<td><xsl:value-of select="@id" /></td>
<td class="left"><xsl:value-of select="first_name" /></td>
<td class="left"><xsl:value-of select="last_name" /></td>
<td class="left"><xsl:value-of select="email" /></td>
<td><xsl:value-of select="phone" /></td>
<td><xsl:value-of select="city" /></td>
<td><xsl:value-of select="country" /></td>
<td><xsl:value-of select="role" /></td>
</tr>
</xsl:for-each>
</table>
<h2>Lecture schedule for course: Full Stack with Java </h2>
<h3>Start date: 10.12.2020 </h3>
<table border="1">
<tr>
<th>Day</th>
<th>Classroom</th>
<th>Capacity</th>
<th>Start time</th>
<th>End time</th>
</tr>
<xsl:for-each select="education_center/course/course_schedule[@start_date='2020-12-10']/lecture_schedule">
<tr>
<td class="left"><xsl:value-of select="@day" /></td>
<td><xsl:value-of select="location" /></td>
<td><xsl:value-of select="capacity" /></td>
<td><xsl:value-of select="start_time" /></td>
<td><xsl:value-of select="end_time" /></td>
</tr>
</xsl:for-each>
</table>
<h2>Taken exams for course: Full Stack with Java </h2>
<h3>Start date: 10.12.2020 </h3>
<table border="1">
<tr>
<th>Participant id</th>
<th>First name</th>
<th>Last name</th>
<th>Exam id</th>
<th>Exam name</th>
<th>Questions</th>
<th>Duration(min.)</th>
<th>Exam place</th>
<th>Time taken</th>
<th>Result in percents</th>
<th>Correct questions</th>
<th>Passed</th>
</tr>
<xsl:for-each select="education_center/course/course_schedule[@start_date='2020-12-10']/participant[role='Student']">
<tr>
<td><xsl:value-of select="@id" /></td>
<td class="left"><xsl:value-of select="first_name" /></td>
<td class="left"><xsl:value-of select="last_name" /></td>
<td><xsl:value-of select="taken_exam/@id" /></td>
<td><xsl:value-of select="taken_exam/exam_name" /></td>
<td><xsl:value-of select="taken_exam/questions" /></td>
<td><xsl:value-of select="taken_exam/duration_min" /></td>
<td><xsl:value-of select="taken_exam/exam_place" /></td>
<td><xsl:value-of select="taken_exam/time_taken" /></td>
<td><xsl:value-of select="taken_exam/result_percents" /></td>
<td><xsl:value-of select="taken_exam/correct_quest" /></td>
<td><xsl:value-of select="taken_exam/passed" /></td>
</tr>
</xsl:for-each>
</table>
<h2>Informations about users</h2>
<table border="1">
<tr>
<th>User id</th>
<th>User name</th>
<th>Password</th>
<th>First name</th>
<th>Last name</th>
<th>Email</th>
<th>Phone</th>
<th>City</th>
<th>Country</th>
<th>Time stamp</th>
<th>Enabled</th>
</tr>
<xsl:for-each select="education_center/course/course_schedule/participant">
<xsl:sort select="@id"/>
<tr>
<td><xsl:value-of select="@id" /></td>
<td class="left"><xsl:value-of select="username" /></td>
<td class="left"><xsl:value-of select="password" /></td>
<td class="left"><xsl:value-of select="first_name" /></td>
<td class="left"><xsl:value-of select="last_name" /></td>
<td class="left"><xsl:value-of select="email" /></td>
<td><xsl:value-of select="phone" /></td>
<td><xsl:value-of select="city" /></td>
<td><xsl:value-of select="country" /></td>
<td><xsl:value-of select="timestamp" /></td>
<td><xsl:value-of select="enabled" /></td>
</tr>
</xsl:for-each>
</table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
